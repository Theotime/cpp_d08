/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mutantstack.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/16 06:57:08 by triviere          #+#    #+#             */
/*   Updated: 2015/01/16 08:00:45 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __MUTANTSTACK_HPP__
# define __MUTANTSTACK_HPP__

# include <iostream>
# include <stack>

template<typename T>
class MutantStack : public std::stack<T> {
	public:

		inline MutantStack() {};
		inline virtual ~MutantStack() {};

		typedef typename std::deque<T>::iterator iterator;
		typedef typename std::deque<T>::const_iterator const_iterator;

		typename std::deque<T>::iterator		begin() { return this->c.begin(); };
		typename std::deque<T>::iterator		end()   { return this->c.end(); };
		typename std::deque<T>::const_iterator	begin()		const { return this->c.begin(); };
		typename std::deque<T>::const_iterator	end()		const { return this->c.end(); };


	private:
		inline MutantStack		&operator=(MutantStack const &o) { *this = o; return (*this); };
		inline MutantStack(MutantStack const &o) { *this = o; };

};

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   span.hpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/16 03:41:55 by triviere          #+#    #+#             */
/*   Updated: 2015/01/16 06:36:05 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __SPAN_HPP__
#define __SPAN_HPP__

#include <iostream>
#include <list>

class Span {

	public:
		Span();
		Span(unsigned int n);
		Span(Span const &o);
		~Span();

		void				addNumber(int const &n);
		void				display();
		int					shortestSpan();
		int					longestSpan();

		Span				&operator=(Span const &o);

	private:
		unsigned int		_n;
		unsigned int		_cur;
		std::list<int>		_all;
};

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   easyfind.hpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/16 01:37:12 by triviere          #+#    #+#             */
/*   Updated: 2015/01/16 03:33:08 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __EASYFIND_HPP__
# define __EASYFIND_HPP__


# include <iostream>
# include <list>
# include <string>

template<typename T>
int			easyfind(T const &c, int const &key) {
	typename T::const_iterator		it;

	for (it = c.begin(); it != c.end() ; ++it) {
		if (*it == key)
			return (*it);
	}
	throw std::string("Not Found");
}

#endif
